package br.ufrn.imd.web2.diariodeclasse.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.ufrn.imd.web2.diariodeclasse.R;
import br.ufrn.imd.web2.diariodeclasse.dominio.NotaAluno;

/**
 * Created by joaomatias on 15/11/16.
 */

public class NotaAlunoListaAdapter extends
        ArrayAdapter<NotaAluno> {

    public NotaAlunoListaAdapter(Context context, int
            textViewResourceId, List<NotaAluno> objects) {
        super(context, textViewResourceId, objects);
    }

    public View getView(int position, View convertView,
                        ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater)
                    getContext().getSystemService
                            (Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_notas,
                    null);
        }
        NotaAluno item = getItem(position);
        if (item!= null) {
            TextView titleText = (TextView)
                    view.findViewById(R.id.idText);
            titleText.setText( item.getId() );
            TextView cnpjText = (TextView)
                    view.findViewById(R.id.notaText);
            cnpjText.setText(item.getNota());
        }
        return view;
    }
}