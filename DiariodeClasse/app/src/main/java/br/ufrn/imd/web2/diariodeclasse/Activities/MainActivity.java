package br.ufrn.imd.web2.diariodeclasse.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.ufrn.imd.web2.diariodeclasse.R;
import br.ufrn.imd.web2.diariodeclasse.dominio.NotaAluno;

public class MainActivity extends AppCompatActivity {

    private List<NotaAluno> notas;
    private EditText idText;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        idText = (EditText) findViewById(R.id.idText);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    private void requestAlunos() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://127.0.0.1:8080/Diario/api/consulta/notas/"
                + idText.getText().toString();

        StringRequest stringRequest = new StringRequest
                (Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                notas = gerarNotasFromJson(response);
                                progressBar.setVisibility(ProgressBar.GONE);
                                Intent i  = new Intent(MainActivity.this, ListActivity.class);
                                i.putExtra("notasAluno", (Serializable) notas);
                                startActivity(i);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                });
        queue.add(stringRequest);
    }

    private List<NotaAluno> gerarNotasFromJson(String dados) {
        List<NotaAluno> resultado = new ArrayList<NotaAluno>();
        try {
            JSONObject jo = new JSONObject(dados);

            NotaAluno n = new NotaAluno();
            try {
                n.setId(jo.getString("id"));
                n.setNota(jo.getString("nota"));
            }catch (JSONException e) {
                e.printStackTrace();
            }
            resultado.add(n);
            return resultado;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultado;
    }

    public void buscar(View view) {
        progressBar.setVisibility(ProgressBar.VISIBLE);
        requestAlunos();
    }
}
