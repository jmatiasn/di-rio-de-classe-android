package br.ufrn.imd.web2.diariodeclasse.dominio;

import java.io.Serializable;

/**
 * Created by joaomatias on 15/11/16.
 */

public class NotaAluno implements Serializable {

    private String id;

    private String nota;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }
}