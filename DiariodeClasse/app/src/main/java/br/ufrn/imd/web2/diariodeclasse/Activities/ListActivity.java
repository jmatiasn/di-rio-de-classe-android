package br.ufrn.imd.web2.diariodeclasse.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.List;

import br.ufrn.imd.web2.diariodeclasse.Adapters.NotaAlunoListaAdapter;
import br.ufrn.imd.web2.diariodeclasse.R;
import br.ufrn.imd.web2.diariodeclasse.dominio.NotaAluno;

/**
 * Created by joaomatias on 15/11/16.
 */

public class ListActivity extends AppCompatActivity {

    private ListView notasListView;
    private NotaAlunoListaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        List<NotaAluno> fornecedores = (List<NotaAluno>)
                getIntent().getSerializableExtra("notasAluno");

        notasListView = (ListView) findViewById(R.id.listalunos);
        adapter = new NotaAlunoListaAdapter(this,
                R.layout.list_notas, fornecedores);

        notasListView.setAdapter(adapter);
    }
}